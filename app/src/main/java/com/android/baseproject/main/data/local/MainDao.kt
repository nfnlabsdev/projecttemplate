package com.android.baseproject.main.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.android.baseproject.base.repo.data.local.BaseDao
import com.android.baseproject.util.model.User

@Dao
interface MainDao : BaseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: User)

}