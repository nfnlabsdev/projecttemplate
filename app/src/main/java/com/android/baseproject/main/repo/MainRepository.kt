package com.android.baseproject.main.repo

import com.android.baseproject.base.repo.BaseRepository
import com.android.baseproject.util.model.User

interface MainRepository : BaseRepository {

    suspend fun loadUser(): User

}