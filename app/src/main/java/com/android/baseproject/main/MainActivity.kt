package com.android.baseproject.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.android.baseproject.BR
import com.android.baseproject.R
import com.android.baseproject.base.BaseActivity
import com.android.baseproject.databinding.ActivityMainBinding
import com.android.baseproject.main.di.injectActivityModules
import com.android.baseproject.util.EventObserver
import com.android.baseproject.util.catchAll
import com.android.baseproject.util.model.CallToAction
import com.android.baseproject.util.model.CallToAction.*
import com.android.baseproject.util.toast
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    override val layoutId: Int
        get() = R.layout.activity_main
    override val bindingVariable: Int
        get() = BR.viewModel
    override val viewModel: MainViewModel by viewModel()

    companion object {
        // Start activity
        fun newIntent(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectActivityModules()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initObserver()
    }

    private fun initObserver() {
        // Navigate
        viewModel.navigationEvent.observe(this, EventObserver {
            catchAll {
                when(it.action) {
                    BACK -> {
                        finish()
                    }
                    else -> { }
                }
            }
        })
    }
}
