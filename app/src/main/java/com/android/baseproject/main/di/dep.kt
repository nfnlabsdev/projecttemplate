package com.android.baseproject.main.di

import com.android.baseproject.di.AppDatabase
import com.android.baseproject.main.MainViewModel
import com.android.baseproject.main.data.local.MainLocalDataSource
import com.android.baseproject.main.data.local.MainLocalDataSourceImpl
import com.android.baseproject.main.data.remote.MainRemoteDataSource
import com.android.baseproject.main.data.remote.MainRemoteDataSourceImpl
import com.android.baseproject.main.data.remote.MainService
import com.android.baseproject.main.repo.MainRepository
import com.android.baseproject.main.repo.MainRepositoryImpl
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit


// Inject module
fun injectActivityModules() = loadFeature

// List of module for injection
private val loadFeature by lazy {
    loadKoinModules(
        listOf(
               viewModelModule,
               repositoryModule,
               dataSourceModule,
               retrofitService,
               roomDao
        )
    )
}

// Provide retrofit service
val retrofitService: Module = module {
    single { get<Retrofit>().create(MainService::class.java) }
}

// Provide room dao
val roomDao: Module = module {
    single { get<AppDatabase>().mainDao() }
}

// Provide remote and local datasource for repo
val dataSourceModule: Module = module {
    single { MainRemoteDataSourceImpl(apolloClient = get(), mainApi = get()) as MainRemoteDataSource }
    single { MainLocalDataSourceImpl(preferenceHelper = get(), mainDao = get()) as MainLocalDataSource }
}

// Provide repo for viewmodel
val repositoryModule: Module = module {
    single { MainRepositoryImpl(remoteDataSource = get(), localDataSource = get()) as MainRepository }
}

val viewModelModule: Module = module {
    viewModel { MainViewModel(mainRepository = get()) }
}
