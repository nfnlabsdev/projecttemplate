@file:SuppressLint("DefaultLocale")

package com.android.baseproject.main.data.remote

import android.annotation.SuppressLint
import com.android.baseproject.base.repo.data.remote.BaseRemoteDataSourceImpl
import com.android.baseproject.util.model.User
import com.apollographql.apollo.ApolloClient

class MainRemoteDataSourceImpl constructor(
    private val apolloClient: ApolloClient,
    private val mainApi: MainService
) : BaseRemoteDataSourceImpl(apolloClient, mainApi), MainRemoteDataSource {

    override suspend fun remoteUser(): User {
        return mainApi.getUser("Username")
    }

}