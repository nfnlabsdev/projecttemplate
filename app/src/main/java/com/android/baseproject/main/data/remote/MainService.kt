package com.android.baseproject.main.data.remote

import com.android.baseproject.base.repo.data.remote.BaseService
import com.android.baseproject.util.model.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface MainService : BaseService {

    @GET("users/{user}/")
    suspend fun getUser(@Path("user") user: String): User

}