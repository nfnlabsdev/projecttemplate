package com.android.baseproject.main

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.android.baseproject.base.BaseViewModel
import com.android.baseproject.main.repo.MainRepository
import com.android.baseproject.util.Event
import com.android.baseproject.util.model.CallToAction
import com.android.baseproject.util.model.Navigate
import com.android.baseproject.util.model.Resource
import com.android.baseproject.util.model.User
import kotlinx.coroutines.launch

class MainViewModel  constructor(
    private val mainRepository : MainRepository
) : BaseViewModel(mainRepository) {

    init {
        doGetUser()
    }

    val userLiveData = MediatorLiveData<Resource<User>>()

    private fun doGetUser() {
        checkNetwork(onNetwork = {
            // Call api in coroutine job
            showLoading(true)
            job = viewModelScope.launch {
                try {
                    mainRepository.loadUser()
                    navigateTologin()
                } catch (e: Exception) {
                    showToast("Something went wrong")
                } finally {
                    showLoading(false)
                }
            }
        }, onOffline = {
            showToast("Your are currently offline")
        })
    }

    private fun navigateTologin() {
        _navigationEvent.value = Event(Navigate(CallToAction.LOGIN, null))
    }
}