package com.android.baseproject.main.data.remote

import com.android.baseproject.base.repo.data.remote.BaseRemoteDataSource
import com.android.baseproject.util.model.User


interface MainRemoteDataSource: BaseRemoteDataSource {

    suspend fun remoteUser() : User
}