package com.android.baseproject.main.repo

import com.android.baseproject.base.repo.BaseRepositoryImpl
import com.android.baseproject.main.data.local.MainLocalDataSource
import com.android.baseproject.main.data.remote.MainRemoteDataSource
import com.android.baseproject.util.model.User

class MainRepositoryImpl constructor (
    val remoteDataSource: MainRemoteDataSource,
    val localDataSource: MainLocalDataSource
) : BaseRepositoryImpl(remoteDataSource, localDataSource), MainRepository {

    // Load user
    override suspend fun loadUser(): User {
         // Get user from remote
         val user = remoteDataSource.remoteUser()
         // Save user in local
         localDataSource.saveUser(user)
         // Load user from local
         return localDataSource.localUser()
    }

}