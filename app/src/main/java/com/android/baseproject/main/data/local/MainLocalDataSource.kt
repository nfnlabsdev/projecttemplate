package com.android.baseproject.main.data.local

import com.android.baseproject.base.repo.data.local.BaseLocalDataSource
import com.android.baseproject.util.model.User


interface MainLocalDataSource : BaseLocalDataSource {

    suspend fun localUser(): User

    suspend fun saveUser(user: User)


}