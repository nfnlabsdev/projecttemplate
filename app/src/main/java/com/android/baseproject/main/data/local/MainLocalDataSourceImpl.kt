package com.android.baseproject.main.data.local

import com.android.baseproject.base.repo.data.local.BaseLocalDataSourceImpl
import com.android.baseproject.util.PreferenceHelper
import com.android.baseproject.util.model.User


class MainLocalDataSourceImpl constructor(
    private val preferenceHelper: PreferenceHelper,
    private val mainDao: MainDao
) : BaseLocalDataSourceImpl(preferenceHelper, mainDao), MainLocalDataSource {

    override suspend fun localUser(): User {
        return mainDao.loadUser("username")
    }

    override suspend fun saveUser(user: User) {
        mainDao.insertUser(user)
    }

}