@file:Suppress("UNCHECKED_CAST")

package com.android.baseproject.util

import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.android.baseproject.R
import com.android.baseproject.base.BaseRecyclerview
import com.android.baseproject.databinding.ViewErrorBinding
import com.android.baseproject.databinding.ViewLoadingBinding
import com.android.baseproject.util.model.*
import com.google.android.material.textfield.TextInputLayout


object BindingAdapters {

    // Load Image from network
    @BindingAdapter(value = ["imageUrl"], requireAll = false)
    @JvmStatic
    fun loadImage(view: ImageView, url: String?) {
        url?.let {
            view.loadImage(it)
        }
    }

    // Screen state based on api and network
    @BindingAdapter(value = ["dataView", "loadingView",
        "errorView", "viewState", "errorType", "pageNumber"], requireAll = false)
    @JvmStatic fun screenVisiblity(rootView: View, dataView: View, loadingView: ViewLoadingBinding,
                                   errorView: ViewErrorBinding, viewState: ResourceState?,
                                   errorType: ErrorType?, pageNumber: Int?
    ) {
        viewState?.let {
            when (it) {
                ResourceState.SUCCESS -> {
                    loadingView.root.gone()
                    errorView.root.gone()
                    rootView.visible()
                    dataView.visible()
                }
                ResourceState.ERROR -> {
                    loadingView.root.gone()
                    dataView.gone()
                    setErrorView(errorType, errorView)
                    errorView.root.visible()
                }
                ResourceState.LOADING -> {
                    // If screen has no recyclerview and
                    // else has and for not first page we show full screen loading
                    if (pageNumber == null) {
                        setLoadView(rootView, dataView, loadingView, errorView)
                    } else if (pageNumber == 0) {
                        setLoadView(rootView, dataView, loadingView, errorView)
                    }
                }
            }
        }
    }

    // Set loading view
    private fun setLoadView(rootView: View, dataView: View, loadingView: ViewLoadingBinding, errorView: ViewErrorBinding) {
        loadingView.root.setBackgroundColor(ContextCompat.getColor(rootView.context, android.R.color.white))
        errorView.root.gone()
        dataView.gone()
        loadingView.root.visible()
    }

    // Set error text and image based on errortype
    private fun setErrorView (errorType: ErrorType?, errorView: ViewErrorBinding) {
        errorType?.let {
            when(it) {
                ErrorType.NO_INTERNET -> {
                    errorView.tvErrorContent.setTextFromString(R.string.no_internet)
                  //  errorView.ivErrorImage.loadDrawable(R.drawable.ic_general_error)
                }
                else -> {
                    errorView.tvErrorContent.setTextFromString(R.string.no_general_error)
                  //  errorView.ivErrorImage.loadDrawable(R.drawable.ic_general_error)
                }
            }
        }
    }

    // Update viewpager adapter
    @BindingAdapter(value = ["updateDataVP"])
    @JvmStatic
    fun updateViewPagerData(view: ViewPager2, list: List<String>?) {
        list?.let {
            //((view.adapter) as CarouselAdapter).setData(list)
        }
    }

    // Update recyclerview adapter
    @BindingAdapter(value = ["updateDataRV"], requireAll = false)
    @JvmStatic
    fun updateRecyclerViewData(view: RecyclerView, resource: Resource<*>?) {
        resource?.let {
            when (it.state) {
                ResourceState.LOADING -> {
                    // Show loading
                    view.post { ((view.adapter) as BaseRecyclerview).addLoader() }
                }
                ResourceState.SUCCESS -> {
                    it.data?.let {
                        // Remove loading
                        ((view.adapter) as BaseRecyclerview).removeLoader()
                        // Update data in adapter
                        ((view.adapter) as BaseRecyclerview).updateItems(it as List<TypeName>)
                    }
                }
                ResourceState.ERROR -> {
                    // Remove loading
                    ((view.adapter) as BaseRecyclerview).removeLoader()
                }
            }
        }
    }

    // Update recyclerview adapter
    @BindingAdapter(value = ["updateDataV"])
    @JvmStatic
    fun updateRecyclerNoPaginationViewData(view: RecyclerView, list: List<TypeName>?) {
        list?.let {
            ((view.adapter) as BaseRecyclerview).updateItems(it)
        }
    }

    fun showError(editText: TextInputLayout, msg: String?) {
        editText.error = msg
    }

    @BindingAdapter(value = ["empty"])
    @JvmStatic
    fun emptyVisible(view: View, list: List<*>?) {
        list?.let {
            if (it.isEmpty()) view.visible()
        }
    }

    // set view visibility
    @BindingAdapter(value = ["visible"])
    @JvmStatic
    fun updateVisible(view: View, event: Event<Boolean>?) {
        event?.peekContent()?.let {
            Log.d("fd", it.toString())
            if (it) view.visible() else view.gone()
        }
    }


}