package com.android.baseproject.util

import android.content.Context
import android.content.SharedPreferences

@Suppress("UNUSED_PARAMETER", "PrivatePropertyName", "unused", "FunctionWithLambdaExpressionBody")
class PreferenceHelper (val context: Context, prefName: String) {

    private val USER_ID = "USER_ID"
    private val USER_TOKEN = "USER_TOKEN"

    private val customPreference: SharedPreferences =
        context.getSharedPreferences(prefName,
            Context.MODE_PRIVATE
        )

    private fun edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editMe = customPreference.edit()
        operation(editMe)
        editMe.apply()
    }

    var userId
        get() = customPreference.getString(USER_ID, "")
        set(value) {
            edit {
                it.putString(USER_ID, value)
            }
        }

    var userToken
        get() = customPreference.getString(USER_TOKEN, "")
        set(value) {
            edit {
                it.putString(USER_TOKEN, value)
            }
        }

    var clearValues
        get() = { }
        set(value) {
            edit {
                it.clear()
            }
        }

    fun clear() {
        customPreference.edit().clear().apply()
    }
}