package com.android.baseproject.util.recyclerview

import androidx.recyclerview.widget.RecyclerView
import com.android.baseproject.databinding.ViewholderEmptyItemBinding
import com.android.baseproject.databinding.ViewholderHeaderItemBinding
import com.android.baseproject.databinding.ViewholderProgressItemBinding

// Commonly used viewholders

// Header
class HeaderViewholder(val binding: ViewholderHeaderItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: String) = with(binding) {

    }
}

// Progress
class ProgressViewholder(val binding: ViewholderProgressItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind() { }
}

// Empty (no view)
class EmptyViewholder(val binding: ViewholderEmptyItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind() { }
}