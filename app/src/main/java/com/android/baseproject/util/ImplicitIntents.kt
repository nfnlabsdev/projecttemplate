package com.mondaymonk.android.util

import android.content.Context
import android.content.Intent
import android.net.Uri

class ImplicitIntents {

    companion object {

        fun dail(context: Context, phoneNo: String) {
            val dialIntent = Intent(Intent.ACTION_DIAL)
            dialIntent.data = Uri.parse("tel:$phoneNo")
            context.startActivity(dialIntent)
        }
    }

}