package com.android.baseproject.util.model

import androidx.databinding.BaseObservable
import kotlinx.android.parcel.Parcelize

// Used for identify multitype in recyclerview
// All data class must extend it.
abstract class TypeName: BaseObservable() {
    abstract var typeName: String
    abstract var id: Int
}