package com.android.baseproject.util.model

// Api wrapper
data class Resource<out T> constructor(
    // Holds current api state
    val state: ResourceState,
    // Holds api response
    val data: T? = null,
    // Holds error msg if found
    val message: String? = null,
    // holds mapped enum
    val errorType: ErrorType? = null
)

sealed class ResourceState {
    object LOADING : ResourceState()
    object SUCCESS : ResourceState()
    object ERROR : ResourceState()
}