package com.android.baseproject.util

import android.animation.Animator
import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.addListener
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.android.baseproject.R
import com.android.baseproject.base.BaseDialogFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy


fun ImageView.loadImage(url: String) {
    Glide.with(context)
        .load(url)
        .placeholder(ColorDrawable(Color.LTGRAY))
        .dontAnimate()
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(this)
}

fun ImageView.loadDrawable(drawable: Int) {
    Glide.with(context)
        .load(drawable)
        .into(this)
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.inVisible() {
    this.visibility = View.INVISIBLE
}


fun TextView.setTextFromString(id: Int) {
    this.text = this.context.getString(id)
}

fun Context.toast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.networkStatus(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (cm != null) {
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
    return false
}

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(((if (currentFocus == null) View(this) else currentFocus)!!))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun RecyclerView.attachSnap() {
    try {
        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(this)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun BaseDialogFragment<*, *>.show(activity: FragmentActivity, tag: String) {
    val fragmentManager = activity.supportFragmentManager
    val transaction = fragmentManager.beginTransaction()
    val prevFragment = fragmentManager.findFragmentByTag(tag)
    if (prevFragment != null) {
        transaction.remove(prevFragment)
    }
    transaction.addToBackStack(null)
    show(transaction, tag)
}

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}

fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int, tag: String? = null){
    supportFragmentManager.inTransaction { add(frameId, fragment, tag) }
}

fun AppCompatActivity.addFragmentBack(fragment: Fragment, frameId: Int){
    supportFragmentManager.inTransaction {
        addToBackStack(null)
        add(frameId, fragment)
    }
}

fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int, tag: String? = null) {
    supportFragmentManager.inTransaction{
        addToBackStack(null)
        setTransition( FragmentTransaction.TRANSIT_FRAGMENT_OPEN )
        replace(frameId, fragment, tag)
    }
}

fun AppCompatActivity.replaceFragmentNoBack(fragment: Fragment, frameId: Int, tag: String) {
    supportFragmentManager.inTransaction{
        setTransition( FragmentTransaction.TRANSIT_FRAGMENT_OPEN )
        replace(frameId, fragment, tag)
    }
}


// Hide system ui like navigation bar and status bar in immersive mode
fun Activity.hideSystemUi() {
    window?.decorView?.systemUiVisibility = (View.SYSTEM_UI_FLAG_FULLSCREEN
            or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_FULLSCREEN
            or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
}

// Show system ui like navigation bar and status bar in immersive mode
fun Activity.showSystemUi() {
    window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
}

fun View.objectAnimateY(value: Float, duration: Long, endListener: (() -> Unit)?) {
    ObjectAnimator.ofFloat(this, "translationY", value).apply {
        this.duration = duration
        addListener( onEnd = { endListener?.invoke() } )
        start()
    }
}

fun View.objectAnimateX(value: Float, duration: Long, endListener: (() -> Unit)?) {
    ObjectAnimator.ofFloat(this, "translationX", value).apply {
        this.duration = duration
        addListener( onEnd = { endListener?.invoke() })
        start()
    }
}

// try catch alternative
inline fun catchAll(message: String? = null, action: () -> Unit?) {
    try {
        action()
    } catch (t: Throwable) {
        Log.e("Failed to $message. ${t.message}", t.toString() )
    }
}