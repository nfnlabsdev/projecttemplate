package com.android.baseproject.util.model

import androidx.room.Entity
import androidx.room.PrimaryKey

// Used to parse user data from shared preference
@Entity
data class User(
    @PrimaryKey
    val userId: String,
    val userName: String?,
    val userToken: String?
)
