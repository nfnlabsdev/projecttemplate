package com.android.baseproject.util.model

// Types of error in screen
enum class ErrorType {
    NO_INTERNET,
    EMPTY_FRANCHISELIST,
    ERROR
}

// View state for screens
enum class ViewState {
    LOADING,
    SUCCESS,
    ERROR
}

// Used for navigation
enum class CallToAction {
    BACK,
    DISMISS,
    LOGIN
}


