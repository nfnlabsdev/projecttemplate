package com.android.baseproject.util.model

import com.android.baseproject.util.model.CallToAction

// Used for observer action from viewmodel to activity
data class Navigate(
    val action : CallToAction,
    val param: Any?
)