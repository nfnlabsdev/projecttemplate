package com.android.baseproject.di

import android.app.Application
import androidx.room.*
import com.android.baseproject.main.data.local.MainDao
import com.android.baseproject.util.model.User

fun roomDB(application: Application): AppDatabase {
    return Room.databaseBuilder(
        application,
        AppDatabase::class.java, "database-base"
    ).build()
}

@Database(entities = [User::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun mainDao(): MainDao

}

