package com.android.baseproject.di

import org.koin.core.context.loadKoinModules


// Inject module
fun injectBaseModules() = loadFeature

// List of module for injection
private val loadFeature by lazy {
    loadKoinModules(
        listOf(
            networkModule,
            localModule
        )
    )
}


