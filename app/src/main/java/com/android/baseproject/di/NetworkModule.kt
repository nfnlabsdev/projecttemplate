package com.android.baseproject.di

import com.android.baseproject.util.PreferenceHelper
import com.apollographql.apollo.ApolloClient
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit


// Provide apolloclient and oauth for datasource
val networkModule: Module = module {
    single { apolloClient(get()) }
    single { retrofitClient() }
}

//Build the Apollo Client
private fun apolloClient(preferenceHelper: PreferenceHelper): ApolloClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger.DEFAULT)
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(httpLoggingInterceptor)
        .addInterceptor { chain ->
            val original = chain.request()
            val builder = original.newBuilder().method(original.method, original.body)
            builder.header("Authorization", "Bearer ${preferenceHelper.userToken}")
            return@addInterceptor chain.proceed(builder.build())
        }
        .build()
        
    return ApolloClient.builder()
            .serverUrl("https://api.github.com/")
            .okHttpClient(okHttpClient)
            .build()
}

//Build the Retrofit client
private fun retrofitClient(): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://api.github.com/")
        .build()
}

