package com.android.baseproject.di


import com.android.baseproject.util.PreferenceHelper
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module

// Provide preference and room for datasource
val localModule: Module = module {

    single {
        PreferenceHelper(
            androidApplication(),
            "BaseProject"
        )
    }

    single(named("userToken")) { getUserToken(get() as PreferenceHelper) }

    single { roomDB(androidApplication()) }
}


fun getUserToken(preferenceHelper: PreferenceHelper) = preferenceHelper.userToken