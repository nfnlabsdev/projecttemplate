package com.android.baseproject.base.repo.data.remote

import com.apollographql.apollo.ApolloClient

// Class used to app level and commonly used remote api
open class BaseRemoteDataSourceImpl constructor(
    private val apolloClient: ApolloClient,
    private val baseService: BaseService
) : BaseRemoteDataSource {

}