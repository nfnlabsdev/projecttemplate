package com.android.baseproject.base

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.baseproject.util.model.TypeName
import com.android.baseproject.util.model.Progress

abstract class BaseRecyclerview : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var lists = mutableListOf<TypeName>()

    fun updateItems(items: List<TypeName>) {
        val diffCallback = BaseDiffCallback(this.lists, items)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.lists.clear()
        this.lists.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }

    fun addLoader() {
        val li = ArrayList(lists)
        li.add(Progress("Loading", -1))
        updateItems(li)
    }

    fun removeLoader() {
        val li = ArrayList(lists)
        if (li.size > 0) li.removeAt(li.size - 1)
        updateItems(li)
    }

    fun clearItems() {
        updateItems(emptyList())
    }

    override fun getItemCount(): Int {
        return lists.size
    }
}

