package com.android.baseproject.base.repo.data.local

import androidx.room.Query
import com.android.baseproject.util.model.User

interface BaseDao {

    @Query("SELECT * FROM USER WHERE userName = :name")
    suspend fun loadUser(name: String): User

}