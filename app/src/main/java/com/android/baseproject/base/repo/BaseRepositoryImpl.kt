package com.android.baseproject.base.repo

import com.android.baseproject.base.repo.data.local.BaseLocalDataSource
import com.android.baseproject.base.repo.data.remote.BaseRemoteDataSource


open class BaseRepositoryImpl (
    private val remoteDataSource : BaseRemoteDataSource,
    private val localDataSource: BaseLocalDataSource
) : BaseRepository {


}