package com.android.baseproject.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.baseproject.base.repo.BaseRepository
import com.android.baseproject.util.Event
import com.android.baseproject.util.model.CallToAction
import com.android.baseproject.util.model.Navigate
import kotlinx.coroutines.Job

open class BaseViewModel(
    private val baseRepository: BaseRepository
) : ViewModel() {

    protected var job: Job? = null

    val isConnected = MutableLiveData<Boolean>().apply { value = true }

    protected val _navigationEvent = MutableLiveData<Event<Navigate>>()
    val navigationEvent: LiveData<Event<Navigate>> = _navigationEvent

    protected val _showToastEvent = MutableLiveData<Event<String?>>()
    val showToastEvent: LiveData<Event<String?>> = _showToastEvent

    val _showLoadingEvent = MutableLiveData<Event<Boolean>>()
    val showLoadingEvent: LiveData<Event<Boolean>> = _showLoadingEvent

    fun showToast(msg: String?) {
        msg?.let {
            _showToastEvent.value = Event(it)
        }
    }

    fun showLoading(flag: Boolean) {
        _showLoadingEvent.value = Event(flag)
    }

    fun navigateToBack() {
        _navigationEvent.value = Event(Navigate(CallToAction.BACK, null))
    }

    fun dismissDialog() {
        _navigationEvent.value = Event(Navigate(CallToAction.DISMISS, null))
    }

    fun checkNetwork(onNetwork: () -> Unit, onOffline: () -> Unit) {
        if (isConnected.value!!) {
            onNetwork()
        } else {
            onOffline()
        }
    }

    fun isNetWorkConnected(flag: Boolean) {
        isConnected.value = flag
    }

    fun commonActions(callToAction: CallToAction, any: Any) {
        _navigationEvent.value = Event(Navigate(callToAction, any))
    }

    override fun onCleared() {
        job?.cancel()
    }
}