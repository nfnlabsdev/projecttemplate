package com.android.baseproject.base.repo.data.remote

import retrofit2.Call
import retrofit2.http.GET

interface BaseService {

    @GET("users/{user}/repos")
    fun refreshToken(token: String): Call<String>

}