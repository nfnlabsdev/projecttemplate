package com.android.baseproject.base


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.android.baseproject.di.injectBaseModules


abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel> : Fragment() {

    var activity: BaseActivity<*, *>? = null
        private set

    private var mRootView: View? = null

    lateinit var binding: T
        private set

    abstract val bindingVariable: Int

    @get:LayoutRes
    abstract val layoutId: Int

    abstract val viewModel: V

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            this.activity = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectBaseModules()
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        mRootView = binding.root
        binding.setVariable(bindingVariable, viewModel)
        binding.lifecycleOwner = this
        return mRootView
    }

    override fun onDetach() {
        activity = null
        super.onDetach()
    }
}