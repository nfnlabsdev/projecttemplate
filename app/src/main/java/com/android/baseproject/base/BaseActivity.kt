package com.android.baseproject.base

import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.android.baseproject.di.injectBaseModules
import com.android.baseproject.main.di.injectActivityModules
import com.android.baseproject.util.networkStatus
import com.android.baseproject.util.toast
import com.android.baseproject.util.EventObserver
import org.koin.ext.getOrCreateScope


abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity() {

    lateinit var binding: T
        private set

    @get:LayoutRes
    abstract val layoutId: Int

    abstract val viewModel: V

    abstract val bindingVariable: Int

    private var handler = Handler()

    private lateinit var connectivityManager: ConnectivityManager

    override fun onCreate(savedInstanceState: Bundle?) {
        injectBaseModules()

        super.onCreate(savedInstanceState)

        performDataBinding()

        // Make Statusbar black prior to android 6
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            window.statusBarColor = Color.BLACK
        }

        init()
    }

    // Start Connectivity Manager and set it livedata for first time
    private fun init() {
        connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        viewModel.isNetWorkConnected(networkStatus())

        //Show error in toast
        viewModel.showToastEvent.observe(this, EventObserver { err ->
            err?.let { toast(it) }
        })

        //Show loading
        viewModel.showLoadingEvent.observe(this, EventObserver {
         //   binding.inlLoader.flLoader.visibility = if (it) View.VISIBLE else View.GONE
        })

    }

    // Binding layout to activity
    private fun performDataBinding() {
        binding = DataBindingUtil.setContentView(this, layoutId)
        binding.setVariable(bindingVariable, viewModel)
        binding.lifecycleOwner = this
        binding.executePendingBindings()
    }

    // Network listener and update on connectivity and set it livedata
    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            runOnUiThread {
                viewModel.isNetWorkConnected(true)
            }
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            runOnUiThread {
                handler.postDelayed({
                    viewModel.isNetWorkConnected(false)
                }, 1000)
            }
        }
    }

    // Register Network Connectivity callback
    override fun onResume() {
        super.onResume()
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                connectivityManager.registerDefaultNetworkCallback(networkCallback)
            } else {
                connectivityManager.registerNetworkCallback(NetworkRequest.Builder().build(), networkCallback)
            }
        } catch (e: Exception) {
            // It will throw exception It is already registered so covered with try/catch
        }
    }

    // Unregister Network Connectivity callback
    override fun onDestroy() {
        super.onDestroy()
        try {
            connectivityManager.unregisterNetworkCallback(networkCallback)
        } catch (e: Exception) {
            // It will throw exception It is unregistered not registered network so covered with try/catch
        }
    }

}