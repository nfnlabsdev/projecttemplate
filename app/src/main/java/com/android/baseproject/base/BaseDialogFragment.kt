package com.android.baseproject.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import com.android.baseproject.di.injectBaseModules


abstract class BaseDialogFragment<T : ViewDataBinding, V : BaseViewModel> : DialogFragment() {

    var activity: BaseActivity<*, *>? = null
        private set

    private var mRootView: View? = null

    lateinit var binding: T
        private set

    @get:LayoutRes
    abstract val layoutId: Int

    abstract val viewModel: V

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            this.activity = context
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectBaseModules()
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        mRootView = binding.root
      //  binding.setVariable(bindingVariable, viewModel)
        binding.lifecycleOwner = this
        return mRootView
    }

    override fun onDetach() {
        activity = null
        super.onDetach()
    }

}