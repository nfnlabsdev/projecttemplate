package com.android.baseproject.base.repo.data.local

import com.android.baseproject.di.AppDatabase
import com.android.baseproject.util.PreferenceHelper
import org.koin.core.KoinComponent
import org.koin.core.inject

// Class used to get and set app level and commonly used data in local
open class BaseLocalDataSourceImpl constructor(
    private val preferenceHelper: PreferenceHelper,
    private val baseDao: BaseDao
) : BaseLocalDataSource, KoinComponent {

    val appDatabase: AppDatabase by inject()

    override fun isUserLoggedIn(): Boolean {
        return preferenceHelper.userToken.isNullOrEmpty()
    }

    override suspend fun clearAppData() {
        // Clear sharedpreference
        preferenceHelper.clear()

        // Clear room db
        appDatabase.clearAllTables()
    }

}