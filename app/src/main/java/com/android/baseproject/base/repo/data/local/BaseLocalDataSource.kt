package com.android.baseproject.base.repo.data.local


interface BaseLocalDataSource {

    fun isUserLoggedIn(): Boolean

    suspend fun clearAppData()

}