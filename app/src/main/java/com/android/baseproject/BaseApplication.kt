package com.android.baseproject

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        //Initialise Koin
        startKoin {
            // Android context
            androidContext(this@BaseApplication)
        }

    }
}